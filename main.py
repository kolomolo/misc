import os
# Flask
from flask import Flask, request, session, g, redirect, url_for, abort, \
         render_template, flash, Markup, send_from_directory, escape

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

@app.route('/device', methods=['GET','POST'])
def device():
    if request.method=='POST':
       checkbox_value = request.form['check']
       brightness = request.form['brightness']
       print("il valore di checkbox: ", checkbox_value)
       print("il valore di brightness: ", brightness)
       #data=Device(checkbox_value,brightness)
       #DataBase.control_device(data)
    return render_template('/device.html')


